﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DemoApp.Common;
using DemoApp.Models;

namespace DemoAppApiClient
{
    /// <summary>
    /// Data service implementation to get pets data from http://agl-developer-test.azurewebsites.net/people.json
    /// </summary>
    public class PetsDataService : IPetsDataService
    {
        private readonly IConfigurationService _configurationService;
        private readonly IHttpHandler _httpHandler;

        public PetsDataService(IConfigurationService configurationService, IHttpHandler httpHandler)
        {
            _configurationService = configurationService;
            _httpHandler = httpHandler;
        }

        /// <summary>
        /// This method gets the pets data using http client
        /// </summary>
        /// <returns>IEnumerable Pets data</returns>
        public async Task<List<PetsData>> GetPetsData()
        {
            try
            {
                string url = _configurationService.GetAppSettings<string>("ApiUrl");

                return await HttpClientCommon.Execute<List<PetsData>>(_httpHandler, url);

            }
            catch
            {
                throw;
            }
        }
    }
}
