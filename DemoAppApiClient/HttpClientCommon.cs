﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DemoAppApiClient
{
    public class HttpClientCommon
    {
        public static async Task<T> Execute<T>(IHttpHandler httpHandler, string url)
        {
            T output;

            HttpResponseMessage response = await httpHandler.GetAsync(url);

            if (response.Content != null)
            {
                var jsonAsString = await response.Content.ReadAsStringAsync();
                output = JsonConvert.DeserializeObject<T>(jsonAsString);
            }
            else
            {
                throw new Exception("Error while getting data from API");
            }

            return output;
        }
    }
}
