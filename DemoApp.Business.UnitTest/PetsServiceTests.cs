﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DemoApp.Models;
using DemoAppApiClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DemoApp.Business.UnitTest
{
    [TestClass]
    public class PetsServiceTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetPetsAndPwner_ThrowsArgumentNullException_WhenPetTypeIsNotProvided()
        {
            var petDataService = new Mock<IPetsDataService>();

            var petsService = new PetsService(petDataService.Object);
            var result = petsService.GetPetsAndOwner(string.Empty);
        }

        [TestMethod]
        public void GetPetsAndOwner_ReturnsOwnersWithCat_When_PetTypeIsCat()
        {
            var petsList = new List<PetsData>
            {
                new PetsData
                {
                    age = 23,
                    gender = "Male",
                    name = "Bob",
                    pets = new List<Pet>()
                    {
                        new Pet
                        {
                            name = "Garfield",
                            type = "Cat"
                        }
                    }
                },
                new PetsData
                {
                    age = 45,
                    gender = "Male",
                    pets = new List<Pet>()
                    {
                        new Pet
                        {
                            name = "Tom",
                            type = "Dog"
                        }
                    }
                }
            };

            var petsDataService = new Mock<IPetsDataService>();
            petsDataService.Setup(c => c.GetPetsData()).Returns(Task.FromResult(petsList));

            var petsService = new PetsService(petsDataService.Object);

            var result = petsService.GetPetsAndOwner("Cat");

            Assert.IsTrue(result.Count == 1);
        }

        [TestMethod]
        public void GetPetsAndOwner_ReturnsEmptyList_When_PetTypeIsNotPresent()
        {
            var petsList = new List<PetsData>
            {
                new PetsData
                {
                    age = 23,
                    gender = "Male",
                    name = "Bob",
                    pets = new List<Pet>()
                    {
                        new Pet
                        {
                            name = "Garfield",
                            type = "Cat"
                        }
                    }
                },
                new PetsData
                {
                    age = 45,
                    gender = "Male",
                    pets = new List<Pet>()
                    {
                        new Pet
                        {
                            name = "Tom",
                            type = "Dog"
                        }
                    }
                }
            };

            var petsDataService = new Mock<IPetsDataService>();
            petsDataService.Setup(c => c.GetPetsData()).Returns(Task.FromResult(petsList));

            var petsService = new PetsService(petsDataService.Object);

            var result = petsService.GetPetsAndOwner("Fish");

            Assert.IsTrue(result.Count == 0);
        }
    }
}
