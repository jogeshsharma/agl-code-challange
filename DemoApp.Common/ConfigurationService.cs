﻿using System;
using System.Configuration;

namespace DemoApp.Common
{
    public class ConfigurationService : IConfigurationService
    {
        public T GetAppSettings<T>(string key)
        {
            var setting = ConfigurationManager.AppSettings[key];
            if (null == setting) return default(T);
            return (T)Convert.ChangeType(setting, typeof(T));
        }
    }
}
