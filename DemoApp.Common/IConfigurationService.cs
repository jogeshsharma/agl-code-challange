﻿using System.Security.Cryptography.X509Certificates;

namespace DemoApp.Common
{
    /// <summary>
    /// Get Application Configuration settings
    /// </summary>
    public interface IConfigurationService
    {
        T GetAppSettings<T>(string key);
    }
}
