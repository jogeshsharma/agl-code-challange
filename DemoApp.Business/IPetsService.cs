﻿using System.Collections.Generic;
using DemoApp.Models;
using DemoAppApiClient;

namespace DemoApp.Business
{
    public interface IPetsService
    {
        List<PetsData> GetPetsAndOwner(string petType);
    }
}
