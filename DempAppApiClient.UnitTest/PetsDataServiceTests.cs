﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using DemoApp.Common;
using DemoAppApiClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DempAppApiClient.UnitTest
{
    [TestClass]
    public class PetsDataServiceTests
    {
        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetPetsData_ThrowsException_When_Data_Null()
        {
            try
            {
                var confiurationService = new Mock<IConfigurationService>();

                confiurationService.Setup(c => c.GetAppSettings<string>(It.IsAny<string>())).Returns(string.Empty);

                var httpHandler = new Mock<IHttpHandler>();
                httpHandler.Setup(c => c.GetAsync(It.IsAny<string>())).ReturnsAsync(new HttpResponseMessage
                {
                    Content = null,
                    StatusCode = HttpStatusCode.NoContent
                });

                var petsDataService = new PetsDataService(confiurationService.Object, httpHandler.Object);
                petsDataService.GetPetsData().Wait();
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        [TestMethod]
        public void GetPetsData_ReturnsPetsCollection_When_DataIsPresent()
        {
            string jsonData =
                "[{\"name\":\"Bob\",\"gender\":\"Male\",\"age\":23,\"pets\":[{\"name\":\"Garfield\",\"type\":\"Cat\"},{\"name\":\"Fido\",\"type\":\"Dog\"}]},{\"name\":\"Jennifer\",\"gender\":\"Female\",\"age\":18,\"pets\":[{\"name\":\"Garfield\",\"type\":\"Cat\"}]}," +
                "{\"name\":\"Steve\",\"gender\":\"Male\",\"age\":45,\"pets\":null},{\"name\":\"Fred\",\"gender\":\"Male\",\"age\":40,\"pets\":[{\"name\":\"Tom\",\"type\":\"Cat\"},{\"name\":\"Max\",\"type\":\"Cat\"},{\"name\":\"Sam\",\"type\":\"Dog\"}," +
                "{\"name\":\"Jim\",\"type\":\"Cat\"}]},{\"name\":\"Samantha\",\"gender\":\"Female\",\"age\":40,\"pets\":[{\"name\":\"Tabby\",\"type\":\"Cat\"}]},{\"name\":\"Alice\",\"gender\":\"Female\",\"age\":64,\"pets\":[{\"name\":\"Simba\",\"type\":\"Cat\"},{\"name\":\"Nemo\",\"type\":\"Fish\"}]}]";

            var confiurationService = new Mock<IConfigurationService>();

            confiurationService.Setup(c => c.GetAppSettings<string>(It.IsAny<string>())).Returns(String.Empty);

            var httpHandler = new Mock<IHttpHandler>();
            httpHandler.Setup(c => c.GetAsync(It.IsAny<string>())).ReturnsAsync(new HttpResponseMessage
            {
                Content = new StringContent(jsonData),
                StatusCode = HttpStatusCode.OK
            });

            var petsDataService = new PetsDataService(confiurationService.Object, httpHandler.Object);

            var petData = Task.Run(async() => await petsDataService.GetPetsData()).Result;

            Assert.IsTrue(petData.Count > 0);
        }
    }
}
